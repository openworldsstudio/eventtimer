extends HBoxContainer

onready var globalVars = get_node("/root/GlobalVariables")

var audioDevices = []
var initAudioDeviceList = false


func _ready():	
	var audioDevices = AudioServer.get_device_list()
	print(str(audioDevices))
	#for audioDevice in audioDevices:
	#	print(str(audioDevice))
	#	audioDevices.append(audioDevice)
	
	# build numbered list of audio devices from globalVars.audioDevices array
	if initAudioDeviceList == false && audioDevices:

		var i=1
		for audioDevice in audioDevices:
			
			var n = LinkButton.new()
			n.text = str(i)
			n.name = str(audioDevice)
			n.connect("pressed", self, "_selectAudioDevice")
			n.enabled_focus_mode = true
	
			self.add_child(n)
			i = i + 1
			
		initAudioDeviceList = true # make sure this only runs once when conditions are favorable



func _process(delta):	
	pass


func _selectAudioDevice():
	var audioDeviceNodes = self.get_children()
	for audioDeviceNode in audioDeviceNodes:
		if audioDeviceNode.pressed:
			var audioDeviceSelected = audioDeviceNode.name
			AudioServer.set_device(str(audioDeviceSelected))
