extends HBoxContainer

onready var globalVars = get_node("/root/GlobalVariables")

var audioDevices
var initAudioDeviceList = false


func _ready():
	
	
	pass


func _process(delta):
	
	# build numbered list of audio devices from globalVars.audioDevices array
	if initAudioDeviceList == false && globalVars.audioDevices:
		audioDevices = globalVars.audioDevices
		var i=1
		for audioDevice in audioDevices:
			
			var n = LinkButton.new()
			n.text = str(i)
			n.name = str(audioDevice)
			n.connect("pressed", self, "_selectAudioDevice")
			n.enabled_focus_mode = true
	
			self.add_child(n)
			i = i + 1
			
		initAudioDeviceList = true # make sure this only runs once when conditions are favorable


func _selectAudioDevice():
	var audioDeviceNodes = self.get_children()
	for audioDeviceNode in audioDeviceNodes:
		if audioDeviceNode.pressed:
			var audioDeviceSelected = audioDeviceNode.name
			AudioServer.set_device(str(audioDeviceSelected))
			
			
			
			
			
			
			
			
			
			
