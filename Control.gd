extends Control

var test = false # true to speed things up for testing :)

var eventStartTime
var hours = 0
var minutes = 0
var seconds = 0
var currentMinutes
var currentSeconds

var eventStartMessage = ""

var eventPending = false

var startHour = 0
var startMinute = 0

var startMinutes = 0
var startSeconds = 0
var hoursUntil = 0
var minutesUntil = 0
var secondsUntil = 0

var eventStatus = "PENDING"

var loud = true


# Called when the node enters the scene tree for the first time.
func _ready():
	$Reset.visible = false
	$Explanation.text = eventStartMessage
	$Duration.text = "30"
	$Seconds.text = "00"
	


func _process(delta):

	if test == false:
		hours = OS.get_time()["hour"] 
		minutes = OS.get_time()["minute"]
		seconds = OS.get_time()["second"]
		
	if test == true:
		# speed up time for testing
		if hours == 0:
			hours = OS.get_time()["hour"] # one time get the current hour
			minutes = OS.get_time()["minute"]
			seconds = OS.get_time()["second"]
		
		if seconds < 60:
			seconds = seconds + 1
		
		if seconds == 60:
			seconds = 0
			if minutes < 60:
				minutes = minutes + 1
			if minutes == 60:
				minutes = 0
				hours = hours + 1
	
	
	
	currentMinutes = hours * 60 + minutes
	currentSeconds = hours * 60 * 60 + minutes * 60 + seconds
	
	
	
	#$Explanation.text = "Time is " + str("%02d" % hours) + ":" + str("%02d" % minutes) + ":" + str("%02d" % seconds) + eventStartMessage
	
	$Explanation.text = "currentSeconds|" + str(currentSeconds) + "|startSeconds|" + str(startSeconds) + "|eventStatus|" + str(eventStatus) + "|secondsUntil|" + str(secondsUntil)
	
	if eventPending:
		#$StartHour.text = "24:24"
		#$StartHour.caret_position = $StartHour.text.length()
		$StartHour.editable = false
	
		# countdown startMinute - currentMinute
		
		if eventStatus == "PENDING":
			$StartHour.add_color_override("font_color_uneditable", Color(0.03,0.73,0.91,1)) # sky blue
			$Seconds.add_color_override("font_color", Color(0.03,0.73,0.91,1)) # sky blue
			startMinutes = startHour * 60 + startMinute
			startSeconds = startMinutes * 60
			secondsUntil = startSeconds - currentSeconds
			minutesUntil = floor(secondsUntil / 60)
			hoursUntil = floor(minutesUntil / 60)
			
			var countdownSeconds = secondsUntil - minutesUntil * 60
			var countdownMinutes = minutesUntil - hoursUntil * 60
			var countdownHours = hoursUntil
			
			#$Explanation.text = "secondsUntil|" + str(secondsUntil) + "|minutesUntil|" + str(minutesUntil) + "|hoursUntil|" + str(hoursUntil)
			
			#$Explanation.text = "currentSeconds|" + str(currentSeconds) + "|startSeconds|" + str(startSeconds) + "|eventStatus|" + str(eventStatus)
	
			$StartHour.text = str("%02d" % countdownHours) + ":" + str("%02d" % countdownMinutes) 
			$Seconds.text = str("%02d" % countdownSeconds)
			
			
			if loud == true && secondsUntil == 3603: # 1 hour			
				if $EventStarting60.playing == false:
					$EventStarting60.play()
			
			if loud == true && secondsUntil == 1803: # 30 minutes
				if $EventStarting30.playing == false:
					$EventStarting30.play()
			
			if loud == true && secondsUntil == 903: # 15 minutes
				if $EventStarting15.playing == false:
					$EventStarting15.play()
			
			if loud == true && secondsUntil == 603: # 10 minutes
				if $EventStarting10.playing == false:
					$EventStarting10.play()
		
			if secondsUntil == 300: # 5 minutes
				if $EventStarting5.playing == false:
					$EventStarting5.play()
			
			if loud == true && secondsUntil == 243: # 4 minutes
				if $EventStarting4.playing == false:
					$EventStarting4.play()
			
			if loud == true && secondsUntil == 183: # 3 minutes
				if $EventStarting3.playing == false:
					$EventStarting3.play()
		
			if loud == true && secondsUntil == 123: # 2 minutes
				if $EventStarting2.playing == false:
					$EventStarting2.play()
					OS.request_attention()
			
			if secondsUntil == 63: # 1 minute
				if $EventStarting1.playing == false:
					$EventStarting1.play()
		
			if secondsUntil == 8: # 5 seconds
				if $EventStarting.playing == false:
					$EventStarting.play()
					OS.request_attention()
			
			
			
			
	
			
			if startSeconds <= currentSeconds:
				eventStatus = "IN PROGRESS"
				# do a one time adjustment to startSeconds
				var durationSeconds = int($Duration.text) * 60
				startSeconds = startSeconds + durationSeconds
				$StartingLabel.text = "Now"
				$StartHour.add_color_override("font_color_uneditable", Color(1,0.7,0,1)) # orange
				$Seconds.add_color_override("font_color", Color(1,0.7,0,1)) # orange
			
				
				
		if eventStatus == "IN PROGRESS":
			secondsUntil = startSeconds - currentSeconds
			minutesUntil = floor(secondsUntil / 60)
			hoursUntil = floor(minutesUntil / 60)
				
			var countdownSeconds = secondsUntil - minutesUntil * 60
			var countdownMinutes = minutesUntil - hoursUntil * 60
			var countdownHours = hoursUntil
	
			$StartHour.text = str("%02d" % countdownHours) + ":" + str("%02d" % countdownMinutes) 
			$Seconds.text = str("%02d" % countdownSeconds)
			
			if secondsUntil == 123: # 2 minutes
				$StartingLabel.text = "Ending"
				if $EventEnding.playing == false:
					$EventEnding.play()
					OS.request_attention()
			
			
			
			
			
			
			
			
			if startSeconds <= currentSeconds:
				eventStatus = "COMPLETE"
				$StartHour.add_color_override("font_color_uneditable", Color(1,1,1.1,1)) # sky blue
				$Seconds.add_color_override("font_color", Color(1,1,1.1,1)) # sky blue
				
		if eventStatus == "COMPLETE":
			$StartingLabel.text = "Ended"
			$StartHour.text = ""
			$Duration.text = ""
			eventPending = false
			OS.request_attention()
			

func _on_StartHour_text_entered(new_text):
	pass
	
	
	


func _on_StartHour_text_changed(new_text):
	
	if eventStatus == "PENDING":
	
		if new_text.length() > 2 && $StartHour.text.count(":",0,5) < 1:
			$StartHour.text = new_text.insert(2,":")
			$StartHour.caret_position = $StartHour.text.length()
			
			
		if new_text.count(":",0,5) == 1:
			# now split to HH and MM
			var time = $StartHour.text.split(":", true)
			
			# first set to string so we can test if we have 2 chars
			var HH = str(time[0])
			var MM = str(time[1])
			
			
			if HH.length() == 2 && MM.length() == 2 && int(HH) <= 24 && int(MM) <= 60 && int(HH) >= 0 && int(MM) >= 0: 
				$Reset.visible = true
				eventStartMessage = " | Event starting @ " + str("%02d" % int(HH)) + ":" + str("%02d" % int(MM))
				startHour = int(HH)
				startMinute = int(MM)
				eventPending = true
			else:
				$Reset.visible = false
				startHour = 0
				startMinute = 0
				eventStartMessage = ""
				eventPending = false
		
		
		
		
		
		
		
		
		
		
		
		



func _on_Reset_gui_input(event):
	if event is InputEventMouseButton:
		var lClick = event.is_action_pressed("LeftClick")
		if lClick:
			get_tree().reload_current_scene()
			pass
			
			if event.is_doubleclick():
				#print("double left click")
				pass
				



func _on_EventName_text_changed(new_text):
	OS.set_window_title(new_text)
